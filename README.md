# Tracking system using .NET and Java web service
The following application is a system for package tracking, implemented in Java and .NET, using web services. The application has a user interface which allows the users to perform operations on the database.
The client can register and then login with the provided credentials. Besides this, the simple user can also:
* See his/her own packages (packages in which he/she represents a sender or a receiver)
* Search for packages (among personal packages)
* Check package status (see the list of places where the package arrived).
The administrator, after loggin in can,
* Insert new packages
* Change package status (to tracking mode)
* Add new tracks (pairs of city and time representing where the package arrived).
## Prerequisites
In order to build and execute the application, you need to have the following resources installed:
* JDK version >= 1.8.0
* Maven version >= 1.0.0
* Tomcat version >= 1.8.0
* Visual Studio version >=2013
* MySQL and MySQL connector for .NET version >=8.0
## Build
In order to build the application, you must build the two maven projects: Java server and client, using Maven install.
```
mvn clean install
```
The Java server application is based on Java Servlets therefore it needs to be run on a server. The server is listed in the prerequisites section: Tomcat 1.8.0.
The .NET application was built using Visual Studio 2013. The IDE takes is integrated with a server, therefore there is no need to execute any special commands for deploying on a server.
## Execute

In order to execute the Java server application run maven with the following instruction (either IDE or terminal).
```
mvn tomcat7:run
```
In order to execute the .NET server application, use Visual studio to run the web application. An HTML page displaying all available web methods will be displayed once the services are succesfully deployed.
In order to execute the client application, run the Main class in package ui of the client application.
```
java Main
```
You must also create a database called trackingsystem and change the log in credentials in class ConnectionFactory of java server application. Run the sql dump file to generate the database.
After the database is created and services are published, you can start performing operations on the graphical user interface of the client. You can run as many clients as needed at a given time.