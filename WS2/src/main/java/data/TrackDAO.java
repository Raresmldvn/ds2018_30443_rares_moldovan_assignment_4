package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import entities.Track;

public class TrackDAO {

	private static final String insertStatement = "INSERT INTO `trackingsystem`.track (package_id, city_id, time) VALUES (?,?,?)";

	public int insertTrack(Track track) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement insert = null;
		ResultSet resSet = null;
		int index = 0;
		try {
			
			insert = connection.prepareStatement(insertStatement);
			insert.setInt(1, track.getPackageId());
			insert.setInt(2, track.getCityId());
			insert.setDate(3, new java.sql.Date(track.getTime().getTime()));
			insert.executeUpdate();
			
		} catch(SQLException e) {

			System.out.println("SQL Error ss: " + e.getMessage() + e.getStackTrace());
		} finally {
			ConnectionFactory.close(resSet);
			ConnectionFactory.close(insert);
			ConnectionFactory.close(connection);
		}
		return index;
	}
}
