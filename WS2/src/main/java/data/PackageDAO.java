package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import entities.Package;

public class PackageDAO {

	private static final String insertStatement = "INSERT INTO `trackingsystem`.package (sender_id, receiver_id, sender_city_id, receiver_city_id, name,description, tracking) VALUES (?,?,?,?,?,?, ?)";
	private static final String updateStatement = "UPDATE `trackingsystem`.package SET tracking=1 WHERE id=?";

	public int insertPackage(Package pack) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement insert = null;
		ResultSet resSet = null;
		int index = 0;
		try {
			
			insert = connection.prepareStatement(insertStatement);
			insert.setInt(1, pack.getSenderId());
			insert.setInt(2, pack.getReceiverId());
			insert.setInt(3, pack.getSenderCityId());
			insert.setInt(4, pack.getReceiverCityId());
			insert.setString(5, pack.getName());
			insert.setString(6, pack.getDescription());
			insert.setBoolean(7, false);
			insert.executeUpdate();
			
		} catch(SQLException e) {

			System.out.println("SQL Error ss: " + e.getMessage() + e.getStackTrace());
		} finally {
			ConnectionFactory.close(resSet);
			ConnectionFactory.close(insert);
			ConnectionFactory.close(connection);
		}
		return index;
	}
	
	public void updatePackage(Package pack) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement update = null;;
		try {
			update = connection.prepareStatement(updateStatement);
			update.setInt(1, pack.getId());
			update.executeUpdate();
		}catch(SQLException e) {
			System.out.println("SQL Error ss: " + e.getMessage() + e.getStackTrace());
		}
		
	}
}
