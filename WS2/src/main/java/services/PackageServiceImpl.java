package services;

import javax.jws.WebService;

import data.PackageDAO;
import entities.Package;

@WebService(endpointInterface = "services.PackageService")
public class PackageServiceImpl implements PackageService {

	public int registerPackage(Package pack) {
		PackageDAO pd = new PackageDAO();
		pd.updatePackage(pack);
		return pack.getId();
	}

	public int addPackage(Package pack) {
		PackageDAO pd = new PackageDAO();
		return pd.insertPackage(pack);
	}

}
