package services;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import entities.Package;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface PackageService {

	@WebMethod
	public int registerPackage(Package pack);
	
	@WebMethod
	public int addPackage(Package pack);
}
