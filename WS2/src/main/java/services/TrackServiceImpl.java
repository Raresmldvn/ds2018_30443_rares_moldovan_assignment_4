package services;

import javax.jws.WebService;

import data.TrackDAO;
import entities.Track;

@WebService(endpointInterface = "services.TrackService")
public class TrackServiceImpl implements TrackService{

	public int addTrack(Track track) {
		TrackDAO td = new TrackDAO();
		return td.insertTrack(track);
	}

}
