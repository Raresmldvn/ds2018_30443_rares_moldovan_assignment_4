package services;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import entities.Track;


@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface TrackService {

	@WebMethod
	public int addTrack(Track track);
	
}
