package entities;

import java.io.Serializable;

public class Package implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int id;
	private int senderId;
	private int receiverId;
	private int senderCityId;
	private int receiverCityId;
	private String name;
	private String description;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSenderId() {
		return senderId;
	}
	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}
	public int getReceiverId() {
		return receiverId;
	}
	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}
	public int getSenderCityId() {
		return senderCityId;
	}
	public void setSenderCityId(int senderCityId) {
		this.senderCityId = senderCityId;
	}
	public int getReceiverCityId() {
		return receiverCityId;
	}
	public void setReceiverCityId(int receiverCityId) {
		this.receiverCityId = receiverCityId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
