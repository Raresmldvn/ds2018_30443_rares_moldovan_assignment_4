﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MySql.Data.MySqlClient;

namespace WebApplication1
{
    /// <summary>
    /// Summary description for UserService
    /// </summary>
    [WebService(Namespace = "http://netservices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class UserService : System.Web.Services.WebService
    {

        [WebMethod]
        public int LogIn(String username, String password)
        {
            MySqlConnection myConnection = new MySqlConnection("Server=localhost;Database=trackingsystem;Uid=root;Pwd=0740115565");
            myConnection.Open();
            string getCity = "SELECT * from user where username=" + MySqlHelper.EscapeString(username);
            MySqlCommand cmd = new MySqlCommand("", myConnection);
            cmd.CommandText = "SELECT * from user where username=@usn";
            cmd.Parameters.AddWithValue("@usn", username);
            MySqlDataReader rdr = cmd.ExecuteReader();
            rdr.Read();
            if (rdr.HasRows == false)
            {
                return 0;
            }
            string realPassword = rdr.GetString("password");
            int id = rdr.GetInt32("id"); 
            myConnection.Close();
            if (password == realPassword)
            {
                return id;
            }
            return 0;
        }

        [WebMethod]
        public int Register(String username, String password)
        {
            MySqlConnection myConnection = new MySqlConnection("Server=localhost;Database=trackingsystem;Uid=root;Pwd=0740115565");
            myConnection.Open();
            MySqlCommand cmd = new MySqlCommand("", myConnection);
            cmd.CommandText = "INSERT INTO user (username, password) VALUES(@usn, @psw)";
            cmd.Parameters.AddWithValue("@usn", username);
            cmd.Parameters.AddWithValue("@psw", password);
            int rowsAffected = cmd.ExecuteNonQuery();
            if (rowsAffected > 0)
            {
                return (int)cmd.LastInsertedId;
            }
            return 0;
        }

        [WebMethod]
        public List<Package> getAllPackages(int userId)
        {
            MySqlConnection myConnection = new MySqlConnection("Server=localhost;Database=trackingsystem;Uid=root;Pwd=0740115565");
            myConnection.Open();
            MySqlCommand cmd = new MySqlCommand("", myConnection);
            if (userId != 0)
            {
                cmd.CommandText = "SELECT * from package where sender_id=@cid OR receiver_id=@rid";
            }
            else
            {
                cmd.CommandText = "SELECT * from package";
            }
            cmd.Parameters.AddWithValue("@cid", userId);
            cmd.Parameters.AddWithValue("@rid", userId);
            MySqlDataReader rdr = cmd.ExecuteReader();
            List<Package> packages = new List<Package>();
            while (rdr.Read())
            {
                int pId = rdr.GetInt32("id");
                User sender = this.getUser(rdr.GetInt32("sender_id"));
                User receiver = this.getUser(rdr.GetInt32("receiver_id"));
                City sendCity = this.getCity(rdr.GetInt32("sender_city_id"));
                City receiveCity = this.getCity(rdr.GetInt32("receiver_city_id"));
                String name = rdr.GetString("name");
                String description = rdr.GetString("description");
                bool tracking = rdr.GetBoolean("tracking");
                List<Track> tracks = this.getAllTracks(pId);
                Package p = new Package(pId, sender, receiver, sendCity, receiveCity, name, description, tracking);
                p.setTracks(tracks);
                packages.Add(p);
            }
            return packages;
        }

        [WebMethod]
        public List<Package> searchPackages(int userId, string search)
        {
            MySqlConnection myConnection = new MySqlConnection("Server=localhost;Database=trackingsystem;Uid=root;Pwd=0740115565");
            myConnection.Open();
            MySqlCommand cmd = new MySqlCommand("", myConnection);
            cmd.CommandText = "SELECT * from package where (sender_id=@cid OR receiver_id=@rid) AND (name LIKE '%" + search + "%')";
            cmd.Parameters.AddWithValue("@cid", userId);
            cmd.Parameters.AddWithValue("@rid", userId);
            Console.Write(cmd.CommandText);
            MySqlDataReader rdr = cmd.ExecuteReader();
            List<Package> packages = new List<Package>();
            while (rdr.Read())
            {
                int pId = rdr.GetInt32("id");
                User sender = this.getUser(rdr.GetInt32("sender_id"));
                User receiver = this.getUser(rdr.GetInt32("receiver_id"));
                City sendCity = this.getCity(rdr.GetInt32("sender_city_id"));
                City receiveCity = this.getCity(rdr.GetInt32("receiver_city_id"));
                String name = rdr.GetString("name");
                String description = rdr.GetString("description");
                bool tracking = rdr.GetBoolean("tracking");
                List<Track> tracks = this.getAllTracks(pId);
                Package p = new Package(pId, sender, receiver, sendCity, receiveCity, name, description, tracking);
                p.setTracks(tracks);
                packages.Add(p);
            }
            return packages;
        }

        [WebMethod]
        public List<City> getAllCities()
        {
            MySqlConnection myConnection = new MySqlConnection("Server=localhost;Database=trackingsystem;Uid=root;Pwd=0740115565");
            myConnection.Open();
            MySqlCommand cmd = new MySqlCommand("", myConnection);
            cmd.CommandText = "SELECT * from city";
            MySqlDataReader rdr = cmd.ExecuteReader();
            List<City> cities = new List<City>();
            while (rdr.Read()) {
                int id = rdr.GetInt32("id");
                String name = rdr.GetString("name");
                String countryName = rdr.GetString("country");
                cities.Add(new City(id, name, countryName));
            }
            return cities;
        }

        [WebMethod]
        public List<User> getAllUsers()
        {
            MySqlConnection myConnection = new MySqlConnection("Server=localhost;Database=trackingsystem;Uid=root;Pwd=0740115565");
            myConnection.Open();
            MySqlCommand cmd = new MySqlCommand("", myConnection);
            cmd.CommandText = "SELECT * from user";
            MySqlDataReader rdr = cmd.ExecuteReader();
            List<User> users = new List<User>();
            while (rdr.Read())
            {
                int id = rdr.GetInt32("id");
                String username = rdr.GetString("username");
                String password = rdr.GetString("password");
                users.Add(new User(id, username, password, false));
            }
            return users;
        }
        private City getCity(int cityId)
        {
            MySqlConnection myConnection = new MySqlConnection("Server=localhost;Database=trackingsystem;Uid=root;Pwd=0740115565");
            myConnection.Open();
            MySqlCommand cmd = new MySqlCommand("", myConnection);
            cmd.CommandText = "SELECT * from city where id=@cid";
            cmd.Parameters.AddWithValue("@cid", cityId);
            MySqlDataReader rdr = cmd.ExecuteReader();
            rdr.Read();
            string name = rdr.GetString("name");
            string countryName = rdr.GetString("country");
            myConnection.Close();
            return new City(cityId, name, countryName);
        }

        private User getUser(int userId)
        {
            MySqlConnection myConnection = new MySqlConnection("Server=localhost;Database=trackingsystem;Uid=root;Pwd=0740115565");
            myConnection.Open();
            MySqlCommand cmd = new MySqlCommand("", myConnection);
            cmd.CommandText = "SELECT * from user where id=@cid";
            cmd.Parameters.AddWithValue("@cid", userId);
            MySqlDataReader rdr = cmd.ExecuteReader();
            rdr.Read();
            string username = rdr.GetString("username");
            string password = rdr.GetString("password");
            bool role = rdr.GetBoolean("role");
            myConnection.Close();
            return new User(userId, username, password, role);
        }

        private List<Track> getAllTracks(int packageId)
        {
            MySqlConnection myConnection = new MySqlConnection("Server=localhost;Database=trackingsystem;Uid=root;Pwd=0740115565");
            myConnection.Open();
            MySqlCommand cmd = new MySqlCommand("", myConnection);
            cmd.CommandText = "SELECT * from track where package_id=@cid";
            cmd.Parameters.AddWithValue("@cid", packageId);
            MySqlDataReader rdr = cmd.ExecuteReader();
            List<Track> tracks = new List<Track>();
            while (rdr.Read())
            {
                int trackId = rdr.GetInt32("id");
                City city = this.getCity(rdr.GetInt32("city_id"));
                DateTime time = rdr.GetDateTime("time");
                tracks.Add(new Track(trackId, city, time));
            }
            myConnection.Close();
            return tracks;
        }
    }

    public class City
    {
        public int id;
        public string name;
        public string countryName;

        public City() { }
        public City(int id, string name, string countryName)
        {
            this.id = id;
            this.name = name;
            this.countryName = countryName;
        }
    }

    public class User
    {
        public int id;
        public string username;
        private string password;
        private bool role;

        public User() { }

        public User(int id, string username, string password, bool role)
        {
            this.id = id;
            this.username = username;
            this.password = password;
            this.role = role;
        }
    }

    public class Package
    {
        public int id;
        public User sender;
        public User receiver;
        public City senderCity;
        public City receiverCity;
        public string name;
        public string description;
        public bool tracking;
        public List<Track> tracks;
        public Package() { }

        public Package(int id, User sender, User receiver, City senderCity, City receiverCity, string name, string description, bool tracking)
        {
            this.id = id;
            this.sender = sender;
            this.receiver = receiver;
            this.senderCity = senderCity;
            this.receiverCity = receiverCity;
            this.name = name;
            this.description = description;
            this.tracking = tracking;
        }

        public void setTracks(List<Track> tracks)
        {
            this.tracks = tracks;
        }
    }

    public class Track
    {
        public int id;
        public City city;
        public DateTime time;

        public Track() { }

        public Track(int id, City city, DateTime time)
        {
            this.id = id;
            this.city = city;
            this.time = time;
        }
    }
}
