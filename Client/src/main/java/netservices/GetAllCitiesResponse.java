
package netservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getAllCitiesResult" type="{http://netservices}ArrayOfCity" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllCitiesResult"
})
@XmlRootElement(name = "getAllCitiesResponse")
public class GetAllCitiesResponse {

    protected ArrayOfCity getAllCitiesResult;

    /**
     * Gets the value of the getAllCitiesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCity }
     *     
     */
    public ArrayOfCity getGetAllCitiesResult() {
        return getAllCitiesResult;
    }

    /**
     * Sets the value of the getAllCitiesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCity }
     *     
     */
    public void setGetAllCitiesResult(ArrayOfCity value) {
        this.getAllCitiesResult = value;
    }

}
