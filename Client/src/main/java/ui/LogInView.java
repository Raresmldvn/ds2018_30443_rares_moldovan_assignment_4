package ui;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

public class LogInView {

	AnchorPane root;
	
	public void load() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader();
			root = fxmlLoader.load(getClass().getResource("/LogIn.fxml").openStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public Scene getScene() {
	     Scene scene = new Scene(root, 500, 280);
	     return scene;
	}
}
