package ui;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

public class PackageView {

AnchorPane root;
	
	public void load() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader();
			root = fxmlLoader.load(getClass().getResource("/Package.fxml").openStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public Scene getScene() {
	     Scene scene = new Scene(root, 600, 300);
	     return scene;
	}
}
