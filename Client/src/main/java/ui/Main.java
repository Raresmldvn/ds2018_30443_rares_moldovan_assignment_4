package ui;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application{

	public void start(Stage primaryStage) throws Exception {
		
		LogInView  view = new LogInView();
		view.load();
		primaryStage.setScene(view.getScene());
		primaryStage.show();
	}
	

	public static void main (String args[]) {
		
		launch(args);
	}
}
