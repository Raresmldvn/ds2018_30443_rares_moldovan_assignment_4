package ui;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import ui.controllers.ClientController;

public class ClientView {

	private AnchorPane root;
	private ClientController controller;
	
	public void load() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader();
			root = fxmlLoader.load(getClass().getResource("/Client.fxml").openStream());
			controller = (ClientController)fxmlLoader.getController();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public ClientController getController() {
		if(controller==null) {
			this.load();
		}
		return controller;
	}
	
	public Scene getScene() {
	     Scene scene = new Scene(root, 750, 450);
	     return scene;
	}
}
