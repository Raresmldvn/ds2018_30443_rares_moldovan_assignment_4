package ui;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import ui.controllers.TrackController;

public class TrackView {

	private AnchorPane root;
	private TrackController controller;
	
	public void load() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader();
			root = fxmlLoader.load(getClass().getResource("/Track.fxml").openStream());
			controller = (TrackController)fxmlLoader.getController();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public TrackController getController() {
		if(controller==null) {
			this.load();
		}
		return controller;
	}
	
	public Scene getScene() {
	     Scene scene = new Scene(root, 400, 500);
	     return scene;
	}
}
