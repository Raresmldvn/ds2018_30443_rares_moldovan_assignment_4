package ui.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import netservices.UserService;
import netservices.UserServiceSoap;
import ui.AdminView;
import ui.ClientView;
import ui.RegisterView;

public class LogInController {

	@FXML
    private TextField usernameTF;

    @FXML
    private PasswordField passwordTF;

    @FXML
    private Button logInButton;

    @FXML
    private Button clearButto;

    @FXML
    void registerEvent(ActionEvent event) {
    	RegisterView regView = new RegisterView();
    	regView.load();
    	Stage stage = (Stage)usernameTF.getScene().getWindow();
    	stage.setScene(regView.getScene());
    }

    @FXML
    void logInEvent(ActionEvent event) {
    	UserService userService = new UserService();
    	UserServiceSoap soap = userService.getUserServiceSoap();
    	if(usernameTF.getText().equals("")||passwordTF.getText().equals("")) {
    		Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText("ERROR");
			alert.setContentText("You must complete all fields!");

			alert.showAndWait();
    	} else {
    		int id = soap.logIn(usernameTF.getText(), passwordTF.getText());
    		if(id==0) {
    			Alert alert = new Alert(AlertType.INFORMATION);
    			alert.setTitle("Information Dialog");
    			alert.setHeaderText("ERROR");
    			alert.setContentText("Username or password are incorrect!");
    			alert.showAndWait();
    		} else {
    			if(id==1) {
    				AdminView adminView = new AdminView();
    				adminView.load();
    		    	Stage stage = (Stage)usernameTF.getScene().getWindow();
    		    	stage.setScene(adminView.getScene());
    			} else {
    				ClientView clientView = new ClientView();
    				ClientController controller = clientView.getController();
    				controller.setUserId(id);
    				Stage stage = (Stage)usernameTF.getScene().getWindow();
    		    	stage.setScene(clientView.getScene());
    			}
    		}
    	}
    }

}
