package ui.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import netservices.UserService;
import netservices.UserServiceSoap;
import ui.ClientView;
import ui.LogInView;

public class RegisterController {

	 @FXML
	 private TextField usernameTF;

	 @FXML
	 private PasswordField passwordTF;

	 @FXML
	 private Button registerButton;

	 @FXML
	 private PasswordField repeatTF;
	 
	 @FXML
	 private Button backButton;

	 @FXML
	 void registerEvent(ActionEvent event) {
		 System.out.println("Here");
		UserService userService = new UserService();
	    UserServiceSoap soap = userService.getUserServiceSoap();
	    if(usernameTF.getText().equals("")||passwordTF.getText().equals("")|| !passwordTF.getText().equals(repeatTF.getText())) {
    		Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText("ERROR");
			alert.setContentText("No empty fields and passwords should match!");

			alert.showAndWait();
    	} else {
    		int registered = soap.register(usernameTF.getText(), passwordTF.getText());
    		if(registered==0) {
    			Alert alert = new Alert(AlertType.INFORMATION);
    			alert.setTitle("Information Dialog");
    			alert.setHeaderText("ERROR");
    			alert.setContentText("Could not register user!");
    		} else {
    			ClientView clientView = new ClientView();
				ClientController controller = clientView.getController();
				controller.setUserId(registered);
				Stage stage = (Stage)usernameTF.getScene().getWindow();
		    	stage.setScene(clientView.getScene());
    		}
    	}
	 }
	 
	 @FXML
	 void backEvent(ActionEvent event) {
		 LogInView  view = new LogInView();
	    	view.load();
	    	Stage stage = (Stage)usernameTF.getScene().getWindow();
	    	stage.setScene(view.getScene());
	 }
}
