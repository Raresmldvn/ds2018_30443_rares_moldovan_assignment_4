package ui.controllers;

import java.time.ZoneId;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import netservices.ArrayOfCity;
import netservices.ArrayOfPackage;
import netservices.City;
import netservices.UserService;
import netservices.UserServiceSoap;
import services.PackageService;
import services.PackageServiceImplService;
import services.track.Track;
import services.track.TrackService;
import services.track.TrackServiceImplService;
import ui.LogInView;
import netservices.Package;

public class PackageController {

	 @FXML
	 private ComboBox<String> packageCombo;

	 @FXML
	 private Button startButton;

	 @FXML
	 private ComboBox<String> cityCombo;

	 @FXML
	 private Button addButton;

	 @FXML
	 private Button logOutButton;

	 @FXML
	 private DatePicker timeDP;

	 @FXML
	 void startEvent(ActionEvent event) {
		 PackageServiceImplService service = new PackageServiceImplService();
		 PackageService soap = service.getPackageServiceImplPort();
		 if(packageCombo.getSelectionModel().getSelectedIndex()==-1) {
			Alert alert = new Alert(AlertType.INFORMATION);
 			alert.setTitle("Information Dialog");
 			alert.setHeaderText("ERROR");
 			alert.setContentText("You must select a package!");
		 }
		 services.Package pack = new services.Package();
		 pack.setId(packages.get(packageCombo.getSelectionModel().getSelectedIndex()).getId());
		 soap.registerPackage(pack);
		 Alert alert = new Alert(AlertType.INFORMATION);
		 alert.setTitle("Information Dialog");
		 alert.setHeaderText("SUCCESS");
		 alert.setContentText("Tracking started!");
		 alert.showAndWait();
	 }
	 
	 @FXML
	 void addEvent(ActionEvent event) {
		 if(packageCombo.getSelectionModel().getSelectedIndex()==-1 
				 || cityCombo.getSelectionModel().getSelectedIndex()==-1
				 || timeDP.getValue() == null) {
				Alert alert = new Alert(AlertType.INFORMATION);
	 			alert.setTitle("Information Dialog");
	 			alert.setHeaderText("ERROR");
	 			alert.setContentText("You must select a package, a city and a date!");
			 }
		 TrackServiceImplService service = new TrackServiceImplService();
		 TrackService soap = service.getTrackServiceImplPort();
		 Track track = new Track();
		 track.setPackageId(packages.get(packageCombo.getSelectionModel().getSelectedIndex()).getId());
		 track.setCityId(cities.get(cityCombo.getSelectionModel().getSelectedIndex()).getId());
		 GregorianCalendar c = GregorianCalendar.from(timeDP.getValue().atStartOfDay(ZoneId.systemDefault()));
		 XMLGregorianCalendar date = null;
		 try {
			date = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 track.setTime(date);
		 soap.addTrack(track);
		 Alert alert = new Alert(AlertType.INFORMATION);
		 alert.setTitle("Information Dialog");
		 alert.setHeaderText("SUCCESS");
		 alert.setContentText("You succesfully added a new route!");
		 alert.showAndWait();
	 }
	 
	 List<Package> packages;
	 List<City> cities;
	 @FXML
	 public void initialize() {
		 UserService userService = new UserService();
		 UserServiceSoap soap = userService.getUserServiceSoap();
		 ArrayOfPackage packageArray = soap.getAllPackages(0);
		 packages = packageArray.getPackage();
		 for(Package pack:packages) {
			 packageCombo.getItems().add(pack.getId() +":" + pack.getName());
		 }
		ArrayOfCity cityArray = soap.getAllCities();
	    cities = cityArray.getCity();
	    for(City city: cities) {
	    	cityCombo.getItems().add(city.getName());
	    }
	 }
	 
	 @FXML
	    void logOutEvent(ActionEvent event) {
	    	LogInView  view = new LogInView();
	    	view.load();
	    	Stage stage = (Stage)timeDP.getScene().getWindow();
	    	stage.setScene(view.getScene());
	    }
}
