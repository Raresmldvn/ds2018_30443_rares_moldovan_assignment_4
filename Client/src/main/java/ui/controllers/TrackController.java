package ui.controllers;

import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

import netservices.Track;
public class TrackController {

	@FXML
    private Label packageLabel;

    @FXML
    private Label senderLabel;

    @FXML
    private Label receiverLabel;

    @FXML
    private Label trackingLabel;

    @FXML
    private ListView<String> routeList;

    @FXML
    private Button backButton;

    public void setPackageName(String name) {
    	packageLabel.setText("Package " + name);
    }
    
    public void setSender(String name) {
    	senderLabel.setText("Sender: " + name);
    }
    
    public void setReceiver(String name) {
    	receiverLabel.setText("Sender: " + name);
    }
    
    public void setTracking(boolean tracked) {
    	trackingLabel.setText("Tracking " + (tracked ? "Enabled" : "Disabled"));
    }
    
    public void setRouteList(List<netservices.Track> tracks) {
    	for(Track track : tracks) {
    		routeList.getItems().add(track.getCity().getName() + " " + track.getTime().getDay() + "." + track.getTime().getMonth() + "." + track.getTime().getYear());
    	}
    }
}
