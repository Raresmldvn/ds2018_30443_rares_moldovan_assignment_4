package ui.controllers;

import java.util.ArrayList;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import netservices.ArrayOfPackage;
import netservices.UserService;
import netservices.UserServiceSoap;
import netservices.Package;
import ui.LogInView;
import ui.TrackView;
import ui.controllers.table.PackageEntry;

public class ClientController {

	private int loggedInId;
	
    @FXML
    private TableView<PackageEntry> packageTable;

    @FXML
    private TableColumn<PackageEntry, Integer> idColumn;

    @FXML
    private TableColumn<PackageEntry, String> senderColumn;

    @FXML
    private TableColumn<PackageEntry, String> receiverColumn;

    @FXML
    private TableColumn<PackageEntry, String> sendCityColumn;

    @FXML
    private TableColumn<PackageEntry, String> receiveCityColumn;

    @FXML
    private TableColumn<PackageEntry, String> nameColumn;

    @FXML
    private TableColumn<PackageEntry, String> descriptionColumn;

    @FXML
    private TextField queryTF;

    @FXML
    private Button logButton;

    @FXML
    private Button searchButton;

    @FXML
    private Button getButton;
    
    @FXML
    private Button openButton;

    List<Package> packages;
    @FXML
    void getEvent(ActionEvent event) {
    	List<PackageEntry> list = new ArrayList<PackageEntry>();
    	UserService userService = new UserService();
    	UserServiceSoap soap = userService.getUserServiceSoap();
    	ArrayOfPackage packageArray = soap.getAllPackages(loggedInId);
    	packages = packageArray.getPackage();
    	for(Package pack : packages) {
    		PackageEntry entry = new PackageEntry();
    		entry.setId(pack.getId());
    		entry.setName(pack.getName());
    		entry.setDescription(pack.getDescription());
    		entry.setSender(pack.getSender().getUsername());
    		entry.setSendCity(pack.getSenderCity().getName());
    		entry.setReceiver(pack.getReceiver().getUsername());
    		entry.setReceiveCity(pack.getReceiverCity().getName());
    		list.add(entry);
    	}
    	populateTable(list);
    }

    @FXML
    void logOutEvent(ActionEvent event) {
    	LogInView  view = new LogInView();
    	view.load();
    	Stage stage = (Stage)queryTF.getScene().getWindow();
    	stage.setScene(view.getScene());
    }

    @FXML
    void searchEvent(ActionEvent event) {
    	List<PackageEntry> list = new ArrayList<PackageEntry>();
    	UserService userService = new UserService();
    	UserServiceSoap soap = userService.getUserServiceSoap();
    	ArrayOfPackage packageArray = soap.searchPackages(loggedInId, queryTF.getText());
    	packages = packageArray.getPackage();
    	for(Package pack : packages) {
    		PackageEntry entry = new PackageEntry();
    		entry.setId(pack.getId());
    		entry.setName(pack.getName());
    		entry.setDescription(pack.getDescription());
    		entry.setSender(pack.getSender().getUsername());
    		entry.setSendCity(pack.getSenderCity().getName());
    		entry.setReceiver(pack.getReceiver().getUsername());
    		entry.setReceiveCity(pack.getReceiverCity().getName());
    		list.add(entry);
    	}
    	populateTable(list);
    }
    
    @FXML
    void openEvent(ActionEvent event) {
    	int selectedId = packageTable.getSelectionModel().getSelectedIndex();
    	if(selectedId==-1) {
    		Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText("ERROR");
			alert.setContentText("You must select a package!");
			alert.showAndWait();
    	} else {
	    	Package pack = packages.get(selectedId);
	    	TrackView trackView = new TrackView();
	    	TrackController controller = trackView.getController();
	    	controller.setPackageName(pack.getName());
	    	controller.setSender(pack.getSender().getUsername() + ", " + pack.getSenderCity().getName());
	    	controller.setReceiver(pack.getReceiver().getUsername() + ", " + pack.getReceiverCity().getName());
	    	controller.setRouteList(pack.getTracks().getTrack());
	    	controller.setTracking(pack.isTracking());
	    	Stage stage = new Stage();
	    	stage.setScene(trackView.getScene());
	    	stage.show();
    	}
    }
    
    public void setUserId(int id) {
    	this.loggedInId = id;
    }
    
    private void populateTable(List<PackageEntry> list) {
    	packageTable.getItems().clear();
    	nameColumn.setCellValueFactory(new PropertyValueFactory<PackageEntry, String>("name"));
    	descriptionColumn.setCellValueFactory(new PropertyValueFactory<PackageEntry, String>("description"));
    	idColumn.setCellValueFactory(new PropertyValueFactory<PackageEntry, Integer>("id"));
    	senderColumn.setCellValueFactory(new PropertyValueFactory<PackageEntry, String>("sender"));
    	receiverColumn.setCellValueFactory(new PropertyValueFactory<PackageEntry, String>("receiver"));
    	sendCityColumn.setCellValueFactory(new PropertyValueFactory<PackageEntry, String>("sendCity"));
    	receiveCityColumn.setCellValueFactory(new PropertyValueFactory<PackageEntry, String>("receiveCity"));
    	packageTable.getItems().addAll(list);
    }
   
}
