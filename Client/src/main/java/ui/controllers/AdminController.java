package ui.controllers;

import java.util.HashMap;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import netservices.ArrayOfCity;
import netservices.ArrayOfUser;
import netservices.City;
import netservices.User;
import netservices.UserService;
import netservices.UserServiceSoap;
import services.PackageService;
import services.PackageServiceImplService;
import ui.LogInView;
import ui.PackageView;

public class AdminController {

	  	@FXML
	    private ComboBox<String> senderUserCombo;

	    @FXML
	    private ComboBox<String> senderCityCombo;

	    @FXML
	    private ComboBox<String> receiverUserCombo;

	    @FXML
	    private ComboBox<String> receiverCityCombo;

	    @FXML
	    private TextField nameTF;

	    @FXML
	    private TextArea descriptionTF;

	    @FXML
	    private Button addButton;

	    @FXML
	    private Button manageButton;
	    
	    @FXML
	    private Button logOutButton;

	    @FXML
	    void addEvent(ActionEvent event) {
	    	PackageServiceImplService service = new PackageServiceImplService();
	    	PackageService soap = service.getPackageServiceImplPort();
	    	services.Package pack = new services.Package();
	    	if(nameTF.getText().equals("") || descriptionTF.getText().equals("")
	    			|| senderUserCombo.getSelectionModel().getSelectedItem() == null
	    			|| receiverUserCombo.getSelectionModel().getSelectedItem() == null
	    			|| senderCityCombo.getSelectionModel().getSelectedItem() == null
	    			|| receiverCityCombo.getSelectionModel().getSelectedItem() == null) {
	    		Alert alert = new Alert(AlertType.INFORMATION);
    			alert.setTitle("Information Dialog");
    			alert.setHeaderText("ERROR");
    			alert.setContentText("You must provide all details!");
    			alert.showAndWait();
	    	} else {
	    		pack.setId(0);
	    		pack.setName(nameTF.getText());
	    		pack.setDescription(descriptionTF.getText());
	    		pack.setSenderId(userMap.get(senderUserCombo.getSelectionModel().getSelectedItem()).intValue());
	    		pack.setReceiverId(userMap.get(receiverUserCombo.getSelectionModel().getSelectedItem()).intValue());
	    		pack.setSenderCityId(cityMap.get(senderCityCombo.getSelectionModel().getSelectedItem()).intValue());
	    		pack.setReceiverCityId(cityMap.get(receiverCityCombo.getSelectionModel().getSelectedItem()).intValue());
	    		int result = soap.addPackage(pack);
	    		if(result>=0) {
	    			Alert alert = new Alert(AlertType.INFORMATION);
	    			alert.setTitle("Information Dialog");
	    			alert.setHeaderText("SUCCESS");
	    			alert.setContentText("You successfully added a new package!");
	    			alert.showAndWait();
	    		}
	    	}
	    }

	    @FXML
	    void manageEvent(ActionEvent event) {
	    	PackageView view = new PackageView();
			view.load();
	    	Stage stage = (Stage)nameTF.getScene().getWindow();
	    	stage.setScene(view.getScene());
	    }
	    
	    @FXML
	    void logOutEvent(ActionEvent event) {
	    	LogInView  view = new LogInView();
	    	view.load();
	    	Stage stage = (Stage)nameTF.getScene().getWindow();
	    	stage.setScene(view.getScene());
	    }
	    
	    @FXML
	    public void initialize() {
	    	getCityOptions();
	    	getUserOptions();
	    }
	    
	    private HashMap<String, Integer> cityMap = new HashMap<String, Integer>();
	 
	    private void getCityOptions() {
	    	UserService userService = new UserService();
	    	UserServiceSoap soap = userService.getUserServiceSoap();
	    	ArrayOfCity cityArray = soap.getAllCities();
	    	List<City> cities = cityArray.getCity();
	    	for(City city : cities) {
	    		senderCityCombo.getItems().add(city.getName());
	    		receiverCityCombo.getItems().add(city.getName());
	    		cityMap.put(city.getName(), city.getId());
	    	}
	    }
	    
	    private HashMap<String, Integer> userMap = new HashMap<String, Integer>();
	   
	    private void getUserOptions() {
	    	UserService userService = new UserService();
	    	UserServiceSoap soap = userService.getUserServiceSoap();
	    	ArrayOfUser userArray = soap.getAllUsers();
	    	List<User> users = userArray.getUser();
	    	for(User user : users) {
	    		senderUserCombo.getItems().add(user.getUsername());
	    		receiverUserCombo.getItems().add(user.getUsername());
	    		userMap.put(user.getUsername(), user.getId());
	    	}
	    }
}
