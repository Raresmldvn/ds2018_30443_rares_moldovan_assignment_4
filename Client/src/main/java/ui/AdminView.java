package ui;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import ui.controllers.AdminController;

public class AdminView {

	private AnchorPane root;
	private AdminController controller;
	public void load() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader();
			root = fxmlLoader.load(getClass().getResource("/Admin.fxml").openStream());
			controller = (AdminController) fxmlLoader.getController();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public AdminController getController() {
		return controller;
	}
	
	public Scene getScene() {
	     Scene scene = new Scene(root, 580, 400);
	     return scene;
	}
}
