
package services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for package complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="package">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="receiverCityId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="receiverId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="senderCityId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="senderId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "package", propOrder = {
    "description",
    "id",
    "name",
    "receiverCityId",
    "receiverId",
    "senderCityId",
    "senderId"
})
public class Package {

    protected String description;
    protected int id;
    protected String name;
    protected int receiverCityId;
    protected int receiverId;
    protected int senderCityId;
    protected int senderId;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the receiverCityId property.
     * 
     */
    public int getReceiverCityId() {
        return receiverCityId;
    }

    /**
     * Sets the value of the receiverCityId property.
     * 
     */
    public void setReceiverCityId(int value) {
        this.receiverCityId = value;
    }

    /**
     * Gets the value of the receiverId property.
     * 
     */
    public int getReceiverId() {
        return receiverId;
    }

    /**
     * Sets the value of the receiverId property.
     * 
     */
    public void setReceiverId(int value) {
        this.receiverId = value;
    }

    /**
     * Gets the value of the senderCityId property.
     * 
     */
    public int getSenderCityId() {
        return senderCityId;
    }

    /**
     * Sets the value of the senderCityId property.
     * 
     */
    public void setSenderCityId(int value) {
        this.senderCityId = value;
    }

    /**
     * Gets the value of the senderId property.
     * 
     */
    public int getSenderId() {
        return senderId;
    }

    /**
     * Sets the value of the senderId property.
     * 
     */
    public void setSenderId(int value) {
        this.senderId = value;
    }

}
